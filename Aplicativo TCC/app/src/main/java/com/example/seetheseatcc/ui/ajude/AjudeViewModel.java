package com.example.seetheseatcc.ui.ajude;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class AjudeViewModel extends ViewModel {

    private MutableLiveData<String> mText;

    public AjudeViewModel()
    {
        mText = new MutableLiveData<>();
        mText.setValue("Tela Ajude");
    }

    public LiveData<String> getText() {
        return mText;
    }
}