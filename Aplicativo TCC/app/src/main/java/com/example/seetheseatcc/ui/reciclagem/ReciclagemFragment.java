package com.example.seetheseatcc.ui.reciclagem;

import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.VideoView;
import android.widget.MediaController;

import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.example.seetheseatcc.R;

public class ReciclagemFragment extends Fragment
{
    //Controle de mídia - Faz o controle dos videos
    private MediaController controleMidia = new MediaController(getActivity());

    @Override //Implementação de um método já criado - Sobrescrita
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) // Método OnCreate -Inicializa a atividade; Bundle - Responsável por guardar o estado da Tela (cache)
    {
        View view = inflater.inflate(R.layout.fragment_reciclagem, container,false);

        VideoView Video1 = (VideoView) view.findViewById(R.id.Video1);

        controleMidia.setAnchorView(Video1);

        //Fazer o link do video (Da pasta raw para o Fragment)
        Uri video = Uri.parse("android.resource://" + getClass().getPackage().getName() + "/" + R.raw.video1);


        //Método set para o vídeo, faz com que o video comece
        Video1.setMediaController(controleMidia);
        Video1.setVideoURI(video);
        Video1.start();

        return view; //Retorna a vizualização do video

    }
}