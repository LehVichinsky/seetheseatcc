package com.example.seetheseatcc.ui.ajude;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.example.seetheseatcc.R;

public class AjudeFragment extends Fragment
{

    private AjudeViewModel AjudeViewModel;

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) // Método OnCreate -Inicializa a atividade; Bundle - Responsável por guardar o estado da Activity (cache)
    {
        AjudeViewModel = ViewModelProviders.of(this).get(AjudeViewModel.class);

        View root = inflater.inflate(R.layout.fragment_ajude, container, false);

        return root;
    }
}