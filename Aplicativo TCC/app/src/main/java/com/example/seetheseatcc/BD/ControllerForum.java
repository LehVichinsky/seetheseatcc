package com.example.seetheseatcc.BD;

import android.content.ContentValues;
import android.content.Context;

import com.example.seetheseatcc.ui.perfil.PerfilFragment;


public class ControllerForum extends DataSource
{

    ContentValues dados;

    public ControllerForum(Context context)
    {
        super(context);

    }

    //Método que recebe um objeto e prepara para enviar para o DataSource e salvar no banco de dados.

    public boolean Salvar(Postagem obj)
    {

        boolean sucesso = true;

        dados = new ContentValues();

        dados.put(Postagem.getIdUsuario(), obj.getIdUsuario());
        dados.put(Postagem.getMensagem(), obj.getMensagem());

        sucesso = insert(Postagem.getTabela(), dados);

        return sucesso;
    }
}

