package com.example.seetheseatcc.BD;

import android.content.ContentValues;
import android.content.Context;

import com.example.seetheseatcc.ui.perfil.PerfilFragment;


public class Controller extends DataSource
{

    ContentValues dados;

    public Controller(Context context)
    {
        super(context);

    }

    //Método que recebe um objeto e prepara para enviar para o DataSource e salvar no banco de dados.

    public boolean Salvar(UsuarioBD obj)
    {

        boolean sucesso = true;

        dados = new ContentValues();

        dados.put(UsuarioBD.getNome(), obj.getNome());
        dados.put(UsuarioBD.getEmail(), obj.getEmail());
        dados.put(UsuarioBD.getSenha(), obj.getSenha());

        sucesso = insert(UsuarioBD.getTabela(), dados);

        return sucesso;
    }
}


