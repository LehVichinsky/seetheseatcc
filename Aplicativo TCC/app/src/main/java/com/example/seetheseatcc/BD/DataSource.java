package com.example.seetheseatcc.BD;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DataSource extends SQLiteOpenHelper {

    //Estrutura para a criação da tabela

    //Criação de atributos
    private static final String DB_NAME = "BDSTS.sqlite";
    private static final int DB_VERSION = 1;

    SQLiteDatabase db; //Classe que permite utilizar as operações de inclusão alteração, exclusão e listagem

    //Esse comando vai passar para a superclasse SQLiteOpenHelper o contexto, nome e versão do BD
    public DataSource(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
        db = getWritableDatabase(); //Permite escrever e ler o BD
    }

    //Método para criar a tabela
    @Override
    public void onCreate(SQLiteDatabase db) {
        try {
            db.execSQL(UsuarioBD.CriarTabela());
        } catch (Exception e) {

        }

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    //Método para inserir na tabela
    public boolean insert(String tabela, ContentValues dados)
    {

        boolean Sucesso = true;
        try {
            Sucesso = db.insert(tabela, null, dados) > 0;
        }

        catch (Exception e)
        {
            Sucesso = false;
        }

        return Sucesso;
    }
}
