package com.example.seetheseatcc.ui.forum;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageButton;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.example.seetheseatcc.BD.ControllerForum;
import com.example.seetheseatcc.BD.Postagem;
import com.example.seetheseatcc.R;

public class ForumFragment extends Fragment
{

    Postagem ForumFragment;
    com.example.seetheseatcc.BD.ControllerForum Controller;

    EditText txtMensagem;
    AppCompatImageButton imgEnviar;

    boolean dadosValidados = true;

    Context context;

    View view;

    public ForumFragment()
    {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) // Método OnCreate -Inicializa a atividade; Bundle - Responsável por guardar o estado da Tela (cache)
    {
        super.onCreate(savedInstanceState);

        context = getContext();

    }
    private ForumViewModel ForumViewModel;

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) // Método OnCreate -Inicializa a atividade; Bundle - Responsável por guardar o estado da Activity (cache)
    {
        ForumViewModel = ViewModelProviders.of(this).get(ForumViewModel.class);

        View root = inflater.inflate(R.layout.fragment_forum, container, false);

        txtMensagem = view.findViewById(R.id.txtMensagem);
        imgEnviar = view.findViewById(R.id.imgEnviar);

        if (dadosValidados)
        {

            ForumFragment = new Postagem(); //Postagem é onde tem as variáveis
            Controller = new ControllerForum(context);
        }

        imgEnviar.setOnClickListener(new View.OnClickListener()
        {
            @Override //Implementação de um método já criado - Sobrescrita
            public void onClick(View v)
            {
                if (Controller.Salvar(ForumFragment))
                {
                    // Objeto salvo com sucesso no DB
                    Toast.makeText(context, "Mensagem enviada com Sucesso...", Toast.LENGTH_LONG).show();
                } else
                {
                    // Falha ao salvar o obj  no DB
                    Toast.makeText(context, "Falha ao enviar a mensagem...", Toast.LENGTH_LONG).show();
                }


            }
        });

        return view;
    }
}