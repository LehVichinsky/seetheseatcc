package com.example.seetheseatcc.ui.home;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.example.seetheseatcc.R;

public class HomeFragment extends Fragment
{

    private HomeViewModel homeViewModel;

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) // Método OnCreate -Inicializa a atividade; Bundle - Responsável por guardar o estado da Tela (cache)
    {
        homeViewModel = ViewModelProviders.of(this).get(HomeViewModel.class);

        View root = inflater.inflate(R.layout.fragment_home, container, false);

        final TextView textView = root.findViewById(R.id.text_tools);

        homeViewModel.getText().observe(this, new Observer<String>()
        {
            @Override //Implementação de um método já criado - Sobrescrita
            public void onChanged(@Nullable String s) {
                textView.setText(s);
            }
        });
        return root;
    }
}