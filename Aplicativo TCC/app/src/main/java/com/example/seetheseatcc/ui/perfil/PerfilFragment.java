package com.example.seetheseatcc.ui.perfil;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.example.seetheseatcc.BD.Controller;
import com.example.seetheseatcc.BD.UsuarioBD;
import com.example.seetheseatcc.ui.perfil.PerfilFragment;
import com.example.seetheseatcc.R;

public class PerfilFragment extends Fragment
{
    UsuarioBD perfilFragment;
    com.example.seetheseatcc.BD.Controller Controller;

    EditText txtNome;
    EditText txtEmail;
    EditText txtSenha;
    Button btnSalvar;

    boolean dadosValidados = true;

    Context context;

    View view;



    public PerfilFragment()
    {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) // Método OnCreate -Inicializa a atividade; Bundle - Responsável por guardar o estado da Tela (cache)
    {
        super.onCreate(savedInstanceState);

        context = getContext();

    }


    private PerfilViewModel PerfilViewModel;


    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        PerfilViewModel = ViewModelProviders.of(this).get(PerfilViewModel.class);
        View root = inflater.inflate(R.layout.fragment_perfil, container, false);

            txtNome = view.findViewById(R.id.txtNome);
            txtEmail = view.findViewById(R.id.txtEmail);
            txtSenha = view.findViewById(R.id.txtSenha);
            btnSalvar = view.findViewById(R.id.btnSalvar);

        if (dadosValidados)
        {

            perfilFragment = new UsuarioBD(); //UsuarioDB é onde tem as variáveis
            Controller = new Controller(context);
        }

        btnSalvar.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                if (Controller.Salvar(perfilFragment))
                {
                    // Objeto salvo com sucesso no DB
                    Toast.makeText(context, "Dados Salvos com Sucesso...", Toast.LENGTH_LONG).show();
                } else
                    {
                    // Falha ao salvar o obj  no DB
                    Toast.makeText(context, "Falha ao salvar os dados do DB...", Toast.LENGTH_LONG).show();
                }


            }
        });

        return view;
    }
}