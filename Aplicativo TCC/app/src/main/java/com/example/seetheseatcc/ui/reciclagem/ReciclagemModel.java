package com.example.seetheseatcc.ui.reciclagem;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class ReciclagemModel extends ViewModel {

    private MutableLiveData<String> mText;

    public ReciclagemModel()
    {
        mText = new MutableLiveData<>();
        mText.setValue("Tela de Reciclagem");
    }

    public LiveData<String> getText() {
        return mText;
    }
}