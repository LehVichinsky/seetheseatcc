package com.example.seetheseatcc.BD;

public class Resposta
{
    private final static String Tabela = "Resposta";

    private final static String idResposta = "idResposta";
    private final static String idUsuario = "idUsuario";
    private final static String idPostagem = "idPostagem";
    private final static String mensagemResposta = "mensagemResposta";

    private static String queryCriarTabela = "";

    //Método para criar a tabela
    public static String CriarTabela()
    {
        queryCriarTabela += "CREATE TABLE" + Tabela;
        queryCriarTabela += " (";
        queryCriarTabela += idResposta + "INTEGER PRIMARY KEY, ";
        queryCriarTabela += idUsuario + "INTEGER FOREIGN KEY, ";
        queryCriarTabela += idPostagem + "INTEGER FOREIGN KEY, ";
        queryCriarTabela += mensagemResposta + "TEXT";
        queryCriarTabela += " )";

        return queryCriarTabela;
    }

    //Métodos para recolher os dados

    public static String getTabela()
    {
        return Tabela;
    }

    public static String getIdResposta()
    {
        return idResposta;
    }

    public static String getIdUsuario()
    {
        return idUsuario;
    }

    public static String getIdPostagem()
    {
        return idPostagem;
    }

    public static String getMensagemResposta()
    {
        return mensagemResposta;
    }

    public static String getQueryCriarTabela()
    {
        return queryCriarTabela;
    }

    public static void setQueryCriarTabela(String queryCriarTabela)
    {
        Resposta.queryCriarTabela = queryCriarTabela;
    }
}
