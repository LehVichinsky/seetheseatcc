package com.example.seethesea;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;


public class Ajude extends AppCompatActivity // Classe Ajude herda a AppCompatActivity
{


    @Override //Implementação de um método já criado - Sobrescrita
    protected void onCreate(Bundle savedInstanceState) // Método OnCreate -Inicializa a atividade; Bundle - Responsável por guardar o estado da Activity (cache)
    {
        super.onCreate(savedInstanceState); // Faz rodar o código junto com o código da classe pai
        setContentView(R.layout.activity_ajude); // É responsável por configurar o layout da Activity

    }
}
