package com.example.seethesea.BD;

public class Postagem
{
    private final static String Tabela = "Postagem";

    private final static String idPostagem = "idPostagem";
    private final static String idUsuario = "idUsuario";
    private final static String mensagem = "mensagem";

    private static String queryCriarTabela = "";

    //Método para criar a tabela
    public static String CriarTabela()
    {
        queryCriarTabela += "CREATE TABLE" + Tabela;
        queryCriarTabela += " (";
        queryCriarTabela += idPostagem + "INTEGER PRIMARY KEY, ";
        queryCriarTabela += idUsuario + "INTEGER FOREIGN KEY, ";
        queryCriarTabela += mensagem + "TEXT";
        queryCriarTabela += " )";

        return queryCriarTabela;
    }

    //Métodos para recolher os dados

    public static String getTabela()
    {
        return Tabela;
    }

    public static String getIdPostagem()
    {
        return idPostagem;
    }

    public static String getIdUsuario()
    {
        return idUsuario;
    }

    public static String getMensagem()
    {
        return mensagem;
    }

    public static String getQueryCriarTabela()
    {
        return queryCriarTabela;
    }

    public static void setQueryCriarTabela(String queryCriarTabela)
    {
        Postagem.queryCriarTabela = queryCriarTabela;
    }


}
