package com.example.seethesea.BD;

public class UsuarioBD
{
    //Dados para criar a tabela no Banco de Dados

    private final static String Tabela = "UsuarioBD";

    private final static String idUsuario = "idUsuario";
    private final static String nome = "nome";
    private final static String email = "email";
    private final static String senha = "senha";

    private static String queryCriarTabela = "";

    //Método para criar a tabela
    public static String CriarTabela()
    {
        queryCriarTabela += "CREATE TABLE" + Tabela;
        queryCriarTabela += " (";
        queryCriarTabela += idUsuario + "INTEGER PRIMARY KEY, ";
        queryCriarTabela += nome + "TEXT, ";
        queryCriarTabela += email + "TEXT, ";
        queryCriarTabela += senha + "TEXT";
        queryCriarTabela += " )";

        return queryCriarTabela;
    }

    //Métodos para recolher os dados

    public static String getTabela()
    {
        return Tabela;
    }

    public static String getIdUsuario()
    {
        return idUsuario;
    }

    public static String getNome()
    {
        return nome;
    }

    public static String getEmail()
    {
        return email;
    }

    public static String getSenha()
    {
        return senha;
    }

    public static String getQueryCriarTabela()
    {
        return queryCriarTabela;
    }

    public static void setQueryCriarTabela(String queryCriarTabela)
    {
        UsuarioBD.queryCriarTabela = queryCriarTabela;
    }


}
