package com.example.seethesea;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;


public class Reciclagem extends AppCompatActivity // Classe TelaReciclagem herda AppCompatActivity
{

    @Override //Implementação de um método já criado - Sobrescrita
    protected void onCreate(Bundle savedInstanceState) // Método OnCreate -Inicializa a atividade; Bundle - Responsável por guardar o estado da Activity (cache)
    {
        super.onCreate(savedInstanceState); // Faz rodar o código junto com o código da classe pai
        setContentView(R.layout.activity_reciclagem); // É responsável por configurar o layout da Activity da Tela Reciclagem
    }
}
